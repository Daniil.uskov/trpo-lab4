using NUnit.Framework;
using TRPO_Lab3.Lib;
using System;

namespace TRPO_Lab3.Tests
{
    public class TestsFormules
    {
        [Test]
        public void TestKonys()
        {
            // arrange
            double h = 5;
            double r = 3;
            double R = 6;
            var expected = 329.69;
            // act
            var V = TRPO_Lab3.Lib.Formules.Konys(R, r, h);

            // assert
            Assert.AreEqual(expected, V, 0.1);
        }

        [Test]
        public void TestNull()
        {
            // arrange
            double h = 5;
            double r = 3;
            double R = 6;
            double V = TRPO_Lab3.Lib.Formules.Konys(R, r, h);
            // act, assert
            if (V <= 0)
            {
                Assert.Fail("����� �� ����� ���� ������ ��� ����� ����");
            }
            else
            {
                Assert.Pass("���� ������� �����");
            }
        }
        [Test]
        public void TestException()
        {
            // arrange
            int a = 5;
            int b = 0;
            // act

            // assert
            Assert.Throws<DivideByZeroException>(() => Formules.DivNull(a,b ));
        }
    }
}