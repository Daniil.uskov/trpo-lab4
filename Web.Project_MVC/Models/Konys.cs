﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Project_MVC.Models
{
    public class Konys
    {
        public double h { get; set; }
        public double R { get; set; }
        public double r1 { get; set; }
        public double V { get; set; }

        public const double Pi = 3.14;

    }
}
