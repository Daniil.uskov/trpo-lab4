﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TRPO_Lab3.Lib;
using Web.Project_MVC.Models;

namespace Web.Project_MVC.Controllers
{
    public class KonysController : Controller
    {
        public IActionResult Konys([FromQuery] Konys request)
        {
            request.V = TRPO_Lab3.Lib.Formules.Konys(request.r1, request.R, request.h);
            return View(request);
        }
    }
}
