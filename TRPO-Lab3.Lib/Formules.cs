﻿using System;

namespace TRPO_Lab3.Lib
{
    public class Formules
    {
        public static double Konys(double R, double r, double h)
            {
            const double Pi = 3.14;
            double V = (1.0 / 3) * Pi * h * ((R * R) + (R * r) + (r * r));
            return V;
            }
        public static double DivNull(int a, int b)
        {
            return a / b;
        }
    }
}
