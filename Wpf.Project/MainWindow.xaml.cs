﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPO_Lab3.Lib;

namespace Wpf.Project
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged 
        
    {
        private double h;
        public double H 
        {
            set
            {
                h = value;
                OnPropertyChanged(nameof(H));
                OnPropertyChanged(nameof(V));
            }
            get => h;
        }
        private double r;
        public double r1
        {
            set
            {
                r = value;
                OnPropertyChanged(nameof(r1));
                OnPropertyChanged(nameof(V));
            }
            get => r;
        }
        private double R;
        public double R1
        {
            set
            {
                R = value;
                OnPropertyChanged(nameof(R1));
                OnPropertyChanged(nameof(V));
            }
            get => R;
        }

        public double V => Formules.Konys(R, r, h);
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
